﻿using System;

namespace TD3
{
    public class Boss 
    {
        public int pointsAttaque; // atr
        private int id; // atr
        string nom ; // atr
        int vitesse; // atr

        public Boss() // constructor
        {
            pointsAttaque = 200;
            int vitesse = 5 ; // var
            string code = nom + id ; // var
            Console.WriteLine(code);
        }
    }

    public abstract class Character
    {
        public int Id { get; protected set; }
        public int Life { get; set; }

        public virtual void TakeDamage(int amount)
        {
            Life -= amount;
        }
    }

    public abstract class CharacterWithAttack : Character
    {
        public int AttackPoint { get; protected set; }
        
        public virtual void Attack(Character character)//méthode
        {
            System.Random rdm = new Random((int)DateTime.Now.Ticks);
            var rdmNumber = rdm.Next(100);
            var attackDamage = rdmNumber % 2 == 0 ? (int) Math.Floor((AttackPoint / 100.0) * rdmNumber) : 0;
            Console.WriteLine($"{this.GetType().Name} attaque : -" + attackDamage);
            character.TakeDamage(attackDamage);
        }
    }

    public class Ennemi : CharacterWithAttack
    {
        public double Speed { get; private set; }

        public Ennemi() 
        {
            Life = 100 ;
            Speed = 5 ;
            AttackPoint = 25 ;
        }

        public void Defend ()//méthode
        { 
            Console.WriteLine("Je me défend") ;
        }

        public void AttackPointInc()//méthode
        { 
            Console.WriteLine($"{this.GetType().Name} prépare son attaque !") ;
            AttackPoint++;
        }
    }

    public class Personnage : Character 
    {
        public double Speed { get; private set; }

        public Personnage () // constructeur
        { 
            Life = 75 ;
            Speed = 15 ;
        }

        public void Heal()
        {
            Life = 75;
        }

        public void Walk ()//méthode
        { 
            Console.WriteLine("Je marche");
        }
        public void Stop ()//méthode
        { 
            Console.WriteLine ("Je m’arrete") ;
        }
    }

    public class Allie : CharacterWithAttack
    {
        public bool IsDefending { get; set; }

        public Allie()
        {
            Life = 75;
            AttackPoint = 10;
            IsDefending = false;
        }

        public override void Attack(Character character)//méthode
        { 
            IsDefending = false;
            base.Attack(character);
        }

        public void DefendPersonnage ()//méthode
        {
            IsDefending = true;
            Console.WriteLine("Défend personnage");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Personnage monPerso = new Personnage();
            Ennemi ennemi1 = new Ennemi() ;
            Allie allie1 = new Allie();
            
            Console.WriteLine($"Vous rencontrer un ennemi!");

            while(monPerso.Life > 0)
            {
                Console.Write($"\nQue faite vous ? ");
                var input = Console.ReadKey();
                Console.WriteLine("");
                
                allie1.IsDefending = false;
                switch (input.Key)
                {
                    case ConsoleKey.H :
                        monPerso.Heal();
                        break;
                    case ConsoleKey.D:
                        if (allie1.Life > 0)
                        {
                            allie1.DefendPersonnage();
                            monPerso.Heal();
                        }
                        break;
                    case ConsoleKey.A:
                        if (allie1.Life > 0)
                        {
                            allie1.Attack(ennemi1);
                        }
                        break;
                }
                    
                ennemi1.Attack(allie1.IsDefending ? (Character)allie1 : (Character)monPerso);
                    
                if (ennemi1.Life <= 0) break;
                
                Console.WriteLine("Vie personnage :" + monPerso.Life);
                Console.WriteLine("Vie allie :" + allie1.Life);
                Console.WriteLine("Vie ennemi :" + ennemi1.Life);
                ennemi1.AttackPointInc() ;
            }
            
            Console.WriteLine($"C'est fini. {(ennemi1.Life <= 0 ? "You win!" : "Game Over!")}");
            Console.ReadKey();
        }
    }
}
