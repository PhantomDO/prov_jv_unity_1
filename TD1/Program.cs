﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Text;


namespace TD1
{
    class Program
    {
        public static void GetInfos()
        {
            string name = "Nicolas", civil = "M.";
            int age = 30;
            StringBuilder sb = new StringBuilder();

            Console.Write($"Name ? ");
            name = Console.ReadLine();
            Console.Write($"Civility ? ");
            civil = Console.ReadLine();
            Console.Write($"Age ? ");

            string str = Console.ReadLine();
            age = Int32.Parse(str.Length > 0 ? str : "-1");

            if (name.Length > 0)
            {
                sb.Append($"Votre prenom est {name} \n");
            }

            if (civil.Length > 0)
            {
                if (civil == "M.") sb.Append($"Vous êtes un homme \n");
                else if (civil == "Mme.") sb.Append($"Vous êtes une femme \n");
            }

            if (age > 0)
            {
                sb.Append($"Vous êtes {(age >= 18 ? "majeur" : "mineur")}\n");
            }

            if (sb.Length <= 0) Console.WriteLine("Error, no input gave");
            else Console.WriteLine(sb.ToString());
        }

        public struct Pizza
        {
            public string Name;
            public float Price;
            public float Radius;
        }

        public static void WhichPizzaIsLessExpensive(Pizza a, Pizza b)
        {
            var pA = a.Radius <= Single.Epsilon ? a.Price : a.Price / a.Radius;
            var pB = b.Radius <= Single.Epsilon ? b.Price : b.Price / b.Radius;

            Pizza lessEnpensive;

            if (Math.Abs(pA - pB) <= Single.Epsilon)
            {
                Console.WriteLine($"The two pizza have the same value.");
            }
            else
            {
                lessEnpensive = pA < pB ? a : b;
                Console.WriteLine($"The less expensive pizza is {lessEnpensive.Name}");
            }
        }

        public static int Factorielle(int n)
        {
            if (n == 0 || n == 1) return 1;

            int r = 1;
            for (int i = 2; i <= n; i++) r *= i;
            
            return r;
        }

        public static int DistinctNumber(int n)
        {
            int nbCoubleDistinct = 0;

            List<(int, int)> list = new List<(int, int)>();

            for (int i = 1; i <= n; i++)
            {
                for (int j = i + 1; j <= n; j++)
                {
                    if (i != j && (i + j) % 2 != 0 && !(list.Contains((i, j)) || list.Contains((j, i))))
                    {
                        list.Add((i, j));
                        nbCoubleDistinct++;
                    }
                }
            }

            foreach (var tuple in list)
            {
                Console.WriteLine($"({tuple.Item1}, {tuple.Item2})");
            }
            return nbCoubleDistinct;
        }

        public static void StringSeparation(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine(str[i]);
            }
        }

        public static string StringWithoutConsonne(string str)
        {
            string baseStr = str;
            char[] charToTrim = 
            { 
                'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'
            };
            foreach (var c in charToTrim)
            {
                if (str.Contains(c))
                {
                    str = str.Replace($"{c}", "");
                }

                if (str.Contains((char) (c - 32)))
                {
                    str = str.Replace($"{(char)(c - 32)}", "");
                }
            }
            Console.WriteLine($"Base: {baseStr}, Trimmed: {str}");
            return str;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            GetInfos();
            WhichPizzaIsLessExpensive(
                new Pizza { Name = "Marguarita", Price = 10f, Radius = 49f },
                new Pizza { Name = "Reine", Price = 8f, Radius = 33f });
            Console.WriteLine($"Factiorielle[10] : {Factorielle(10)}");
            Console.WriteLine($"There is {DistinctNumber(6)} distinct couple number for 6");
            StringSeparation("La vie est belle!");
            Debug.Assert("I ai ouou eau !" == StringWithoutConsonne("Il fait toujours beau !"));
        }
    }
}
